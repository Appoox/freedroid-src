---------------------------------------------------------------------
--
--  Copyright (c) 2002, 2003 Johannes Prix
--  Copyright (c) 2003 Pete Spicer
--
-- This file is part of Freedroid
--
-- Freedroid is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- Freedroid is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Freedroid; see the file COPYING. If not, write to the
-- Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
-- MA 02111-1307 USA
----------------------------------------------------------------------

title_screen{
song = "Bleostrada.ogg",
text = [[
            CONTROLES

MOVIMENTO:

O jogo é controlado usando o mouse. Clique com o botão esquerdo nos locais para os quais você quer ir. Mantenha o botão pressionado para se manter em movimento. Segurar a tecla CTRL irá fazer você correr.

ATAQUE:

Clicar com o botão esquerdo em um inimigo fará com que Tux se aproxime do robô e comece a atacá-lo com a arma atual. Se a arma atual for uma arma de longa distância então Tux ficará parado e atirará no robô.

FUNÇÕES ESPECIAIS:

Use o botão direito do mouse para ativar a habilidade/programa atualmente selecionado.

Teclas padrão (a maioria pode ser modificada na lista de atalhos - F1)

  Escape ... Mostra o menu principal
  F1 ... Mostra a lista de atalhos
  F2 ... Habilita a tela cheia (apenas Linux)
  F3 ... Salvar
  F4 ... Carregar
  F5-F12 ... Selecionar rapidamente programas de 1 até 8

  I ... Painel do inventário
  C ... Painel do personagem
  S - Painel de programas/habilidades
  Espaço - Fecha todos os painéis abertos
  Q ... Registro de 'quests'
  0-9 ... Utilizar os itens do inventário rápido 0-9

  P ... Pausar
  Segura A ... Ataca enquanto parado na mesma posição
  Segure Shoft ... Mantem o alvo selecionado enquanto se move
  Tab ... Alternar automapa

  R ... Recarregar arma
  Manter Ctrl ... Correr
  U ... Correr sempre

  Segure
  Z          ... Mostrar rórulos dos itens no chão 
  T          ... Paredes ficam transparentes quando Tux se aproxima (apenas em OpenGL)

TAKEOVER/CHAT

Para conversar com um robô amigável, basta clicar nele. Para controlar um robô hostil, selecione a habilidade de hack na tela de habilidades, e clique com o botão direito para iniciar o takeover.

Para o jogo de hackear, você pode usar a roda do mouse ou as teclas direcionais.

Você pode se interessar em jogar o Tutorial, acessivel no menu principal.

Se você precisar de ajuda ou tiver problemas, sinta-se livre para vir em #freedroid em irc.freenode.net e pedir ajuda.

Isso é tudo, tenha um bom jogo.



Aperte o botão esquerdo do mouse para começar a jogar.
]]
}

---------------------------------------------------------------------
--
--  Copyright (c) 2002, 2003 Johannes Prix
--
-- This file is part of Freedroid
--
-- Freedroid is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- Freedroid is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Freedroid; see the file COPYING. If not, write to the
-- Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
-- MA 02111-1307 USA
----------------------------------------------------------------------

title_screen{
background = "credits.jpg",
song = "HellFortressTwo.ogg",
voice_acting = "EndOfAct.ogg",
preroll_text = 2,
postroll_text = 5,
text = [[
    Bavíš se?
  Já ne.

Roboti se zdají být kvůli něčemu velmi naštvaný.
Předpokládám, že jsou rozhněvaný na tebe.
Alespoň mě přestaly hledat.
Quite a? Relief?

V tvé oblasti je umělá inteligence.
Říká si "Singularita".
Nebo tak nějak.
Nechápu, co to chce.
Budu tě informovat.

Oh a mimochodem...
I'm still waiting for you.
I haven't moved an inch since you woke up.
Now that the beloved town of yours is safe...
...Wouldn't it be a good time to save me as well?

            Dvořák, První umělá inteligence.



=== END OF ACT 1 ===
]]
}
